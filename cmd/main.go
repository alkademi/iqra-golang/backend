package main

import (
	"iqra/src/account"
	"iqra/src/config"
	"iqra/src/dictionary"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	e := echo.New() // init web framework
	e.Validator = &CustomValidator{validator: validator.New()}

	middlewareAuth := middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(config.GetEnvVar("JWT_SIGNATURE_KEY")),
	})

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: true,
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{http.MethodPost, http.MethodOptions, http.MethodGet},
		AllowHeaders:     []string{"Accept", "Content-Type", "Content-Length", "Accept-Encoding", "CUSTOM-ALLOWED-HEADER", "Authorization", "Access-Key", "X-CSRF-Token,"},
	}))

	e.POST("/account/create", account.CreateAccount())
	e.POST("/account/validate", account.ValidateAccount())
	e.POST("/account/get_one", account.GetOneAccount())
	e.GET("/account/me", account.SelfAccount(), middlewareAuth)
	e.POST("/account/reset_password", account.ResetPassword())
	e.POST("/account/add_exp", account.AddExp())
	e.POST("/account/change_password", account.ChangePassword())
	e.POST("/account/delete", account.DeleteAccount())

	e.POST("/login", account.Login())
	e.GET("/leaderboard", account.GetLeaderboard())

	e.POST("/dict/get-words", dictionary.SearchWordsByWord())
	e.POST("/dict/get-words-id", dictionary.SearchWordsById())

	e.POST("/dict/get-learned", dictionary.SearchWordsLearned())
	e.POST("/dict/add-learned", dictionary.AddLearned())
	e.POST("/dict/rem-learned", dictionary.RemLearned())
	e.POST("/dict/get-flashcard-random", dictionary.SearchRandomWords())

	e.Logger.Fatal(e.Start(":8080"))
}
