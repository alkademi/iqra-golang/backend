package account

import (
	"context"
	"fmt"
	"log"

	"iqra/src/model"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

type repository struct {
	DB *sqlx.DB
}

func newRepository(db *sqlx.DB) *repository {
	return &repository{
		DB: db}
}

func (r *repository) GetOneAccount(ctx context.Context, req RequestSearchAccount) (res model.Account, err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	qb := psq.Select(
		"username",
		"password",
		"full_name",
		"email",
		"birth_date",
		"exp",
		"word_learned",
		"is_validated",
		"regis_token",
		"rank").
		From("(SELECT username,password,full_name,email,birth_date,exp,word_learned,is_validated,regis_token,RANK () OVER (ORDER BY exp DESC, username ASC) rank FROM account)a")

	if req.Username != "" {
		qb = qb.Where("username = ?", req.Username)
	}

	if req.Email != "" {
		qb = qb.Where("email = ?", req.Email)
	}

	query, args, _ := qb.ToSql()
	err = r.DB.GetContext(ctx, &res, query, args...)

	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (r *repository) GetManyAccount(ctx context.Context, req RequestSearchAccount) (res []model.Account, err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	qb := psq.Select(
		"username",
		"password",
		"full_name",
		"email",
		"birth_date",
		"exp",
		"word_learned",
		"is_validated",
		"regis_token",
		"rank").
		From("(SELECT username,password,full_name,email,birth_date,exp,word_learned,is_validated,regis_token,RANK () OVER (ORDER BY exp DESC) rank FROM account)a")

	if req.SortExp == 1 {
		qb = qb.OrderBy("exp DESC, username ASC")
	}

	if req.Limit > 0 {
		qb = qb.Limit(uint64(req.Limit))
	}

	query, args, _ := qb.ToSql()
	err = r.DB.SelectContext(ctx, &res, query, args...)

	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (r *repository) CreateAccount(c context.Context, acc model.Account) (err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	query, args, _ := psq.Insert("account").Columns(
		"username",
		"password",
		"full_name",
		"email",
		"birth_date",
		"exp",
		"word_learned",
		"is_validated",
		"regis_token").
		Values(
			acc.Username,
			acc.Password,
			acc.FullName,
			acc.Email,
			acc.BirthDate,
			acc.Exp,
			acc.Word,
			acc.IsValidated,
			acc.RegisToken).ToSql()

	fmt.Println(acc.Password)
	fmt.Println(query)

	tx, err := r.DB.BeginTxx(c, nil)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = tx.ExecContext(c, query, args...)
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) UpdateAccount(c context.Context, acc model.Account) (err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	query, args, _ := psq.Update("account").
		Set("password", acc.Password).
		Set("full_name", acc.FullName).
		Set("exp", acc.Exp).
		Set("word_learned", acc.Word).
		Set("is_validated", acc.IsValidated).
		Where("username = ?", acc.Username).
		ToSql()

	tx, err := r.DB.BeginTxx(c, nil)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = tx.ExecContext(c, query, args...)
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) DeleteAccount(c context.Context, payload RequestUpdateAccount) (err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)

	query, args, _ := psq.Delete("account").Where("username = ?", payload.Username).ToSql()

	tx, err := r.DB.BeginTxx(c, nil)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = tx.ExecContext(c, query, args...)
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) CheckRegisTokenAvail(c context.Context, token string) (val bool, err error) {

	row := r.DB.QueryRow("SELECT 1 FROM account WHERE regis_token = $1 AND is_validated=0 UNION ALL SELECT 0 LIMIT 1", token)

	var isAvail int

	err = row.Scan(&isAvail)
	if err != nil {
		log.Println(err)
		return
	}

	val = isAvail == 1
	return
}

func (r *repository) GetUserRank(c context.Context, payload RequestSearchAccount) (rank int, err error) {
	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query, args, _ := psq.Select(
		"ROW_NUMBER() OVER(ORDER BY exp DESC, username ASC) AS rank").
		From("account").Where("username = ?", payload.Username).ToSql()

	err = r.DB.GetContext(c, &rank, query, args...)

	if err != nil {
		log.Println(err)
		return
	}

	return
}
