package account

import (
	"iqra/src/dictionary"
	"log"
)

var (
	agent     Agent
	dictAgent dictionary.DictAgent
)

func init() {
	log.SetFlags(log.Llongfile)

	log.Println("Account Initializing")

	agent = NewService()
	dictAgent = dictionary.NewService()

	log.Println("Account Initialiazed")
}
