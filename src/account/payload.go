package account

type RequestSearchAccount struct {
	Username string `json:"username" db:"username" validate:"omitempty"`
	Email    string `json:"email" db:"email" validate:"omitempty"`
	SortExp  int    `json:"sort_exp" validate:"omitempty"`
	Limit    int    `json:"limit" validate:"omitempty"`
}

type RequestCreateAccount struct {
	Username  string `db:"username" json:"username" validate:"required,min=2,max=30"`
	Password  string `db:"password" json:"password" validate:"required,min=8,max=100"`
	FullName  string `db:"full_name" json:"full_name" validate:"required"`
	Email     string `db:"email" json:"email" validate:"required"`
	BirthDate string `db:"birth_date" json:"birth_date" validate:"required"`
}

type RequestLogin struct {
	Username string `db:"username" json:"username" validate:"required,min=2,max=30"`
	Password string `db:"password" json:"password" validate:"required,min=8,max=100"`
}

type RequestUpdateAccount struct {
	Username    string `db:"username" json:"username" validate:"required,max=30"`
	Password    string `db:"password" json:"password" validate:"omitempty,min=8,max=100"`
	FullName    string `db:"full_name" json:"full_name" validate:"omitempty"`
	Exp         int32  `db:"exp" json:"exp" validate:"omitempty"`
	Word        int32  `db:"word_learned" json:"word_learned" validate:"omitempty"`
	IsValidated int    `db:"is_validated" json:"is_validated" validate:"omitempty"`
}

type RequestAddExp struct {
	Username string `db:"username" json:"username" validate:"required,min=2,max=30"`
	Exp      int32  `db:"exp" json:"exp" validate:"required"`
}

type RequestAddExpLearned struct {
	Username   string `db:"username" json:"username" validate:"required,min=2,max=30"`
	DictId     int    `json:"dict_id" validate:"required"`
	NativeLang string `json:"native_lang" validate:"required"`
	TargetLang string `json:"target_lang" validate:"required"`
}

type RequestValidateAccount struct {
	Username   string `db:"username" json:"username" validate:"required,max=30"`
	RegisToken string `db:"regis_token" json:"regis_token" validate:"required"`
}

type RequestChangePassword struct {
	Username    string `db:"username" json:"username" validate:"required,max=30"`
	OldPassword string `json:"old_password" validate:"required,min=8,max=100"`
	NewPassword string `json:"new_password" validate:"required,min=8,max=100"`
}

type RequestResetPassword struct {
	Username string `db:"username" json:"username" validate:"required,max=30"`
	Email    string `json:"email" db:"email" validate:"required"`
}
