package account

import (
	"context"
	"database/sql"
	"fmt"
	"iqra/src/config"
	"iqra/src/model"
	"iqra/src/utils"
	"log"

	jwt "github.com/golang-jwt/jwt/v4"
)

type service struct {
	repository *repository
}

type Agent interface {
	SearchOneAccount(ctx context.Context, payload RequestSearchAccount) (data model.Account, err error)
	CreateAccount(ctx context.Context, payload RequestCreateAccount) (data model.Account, err error)
	Login(ctx context.Context, payload RequestLogin) (userToken map[string]string, err error)
	UpdateAccount(ctx context.Context, payload RequestUpdateAccount) (data model.Account, err error)
	AddExp(ctx context.Context, payload RequestAddExp) (data model.Account, err error)
	ValidateAccount(ctx context.Context, payload RequestValidateAccount) (data model.Account, err error)
	GetLeaderboard(ctx context.Context) (data []model.Account, err error)
	ResetPassword(ctx context.Context, payload RequestResetPassword) (data model.Account, err error)
	ChangePassword(ctx context.Context, payload RequestChangePassword) (data model.Account, err error)
	DeleteAccount(ctx context.Context, payload RequestUpdateAccount) (err error)
	GetUserRank(c context.Context, payload RequestSearchAccount) (rank int, err error)
}

func NewService() Agent {
	db := config.GetDB()
	repo := newRepository(db)
	return &service{
		repository: repo,
	}
}

func (s *service) SearchOneAccount(ctx context.Context, payload RequestSearchAccount) (data model.Account, err error) {

	data, err = s.repository.GetOneAccount(ctx, payload)

	return
}

func (s *service) CreateAccount(ctx context.Context, payload RequestCreateAccount) (data model.Account, err error) {

	search_email := RequestSearchAccount{
		Email: payload.Email,
	}

	_, err = s.SearchOneAccount(ctx, search_email)

	if err != nil && err != sql.ErrNoRows {
		log.Println(err)
		return
	}

	if err == nil {
		err = fmt.Errorf("akun dengan email ini sudah ada")
		log.Println(err)
		return
	}

	search := RequestSearchAccount{
		Username: payload.Username,
	}

	_, err = s.SearchOneAccount(ctx, search)

	if err != nil {
		if err == sql.ErrNoRows {
			//valid

			//generate validation token
			token := utils.GenerateRegisToken()

			for isTokenAvailable, _ := s.repository.CheckRegisTokenAvail(ctx, token); isTokenAvailable; isTokenAvailable = false {
				token = utils.GenerateRegisToken()
			}

			// acc.Password, _ = config.HashPassword(acc.Password)
			hashedPassword, _ := config.HashPassword(payload.Password)

			acc := model.Account{
				Username:    payload.Username,
				Password:    hashedPassword,
				FullName:    payload.FullName,
				Email:       payload.Email,
				BirthDate:   payload.BirthDate,
				Exp:         0,
				Word:        0,
				IsValidated: 0,
				RegisToken:  token,
			}

			err = s.repository.CreateAccount(ctx, acc)
			if err != nil {
				log.Println(err)
				return
			}
			//berhasil
			data = acc

			//send email
			err = utils.SendAccountValidationMail(acc.Email, acc.FullName, token)
			if err != nil {
				log.Println(err)
				return
			}
			return
		}

		log.Println(err)
		return
	}

	err = fmt.Errorf("username tidak tersedia")
	return
}

func (s *service) ValidateAccount(ctx context.Context, payload RequestValidateAccount) (data model.Account, err error) {
	searchReq := RequestSearchAccount{
		Username: payload.Username,
	}

	temp, _ := s.SearchOneAccount(ctx, searchReq)
	if temp.IsValidated == 1 {
		err = fmt.Errorf("akun ini sudah divalidasi")
		return
	}

	if payload.RegisToken == temp.RegisToken {
		updateReq := RequestUpdateAccount{
			Username:    payload.Username,
			IsValidated: 1,
		}

		data, err = s.UpdateAccount(ctx, updateReq)
		if err != nil {
			log.Println(err)
			return
		}
		return
	}
	err = fmt.Errorf("token validasi salah")
	return
}

func (s *service) Login(ctx context.Context, payload RequestLogin) (userToken map[string]string, err error) {
	search := RequestSearchAccount{
		Username: payload.Username,
	}

	acc, err := s.SearchOneAccount(ctx, search)

	if err != nil {
		if err == sql.ErrNoRows {
			err = fmt.Errorf("akun tidak ditemukan")
		}
		log.Println(err)
		return
	}

	if acc.IsValidated == 0 {
		err = fmt.Errorf("akun belum divalidasi")
		return
	}

	if config.CheckPasswordHash(payload.Password, acc.Password) {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": acc.Username,
			"email":    acc.Email,
		})

		signedToken, _ := token.SignedString([]byte(config.GetEnvVar("JWT_SIGNATURE_KEY")))

		userToken = map[string]string{
			"token": signedToken,
		}

		return
	}

	err = fmt.Errorf("password salah")
	return
}

func (s *service) UpdateAccount(ctx context.Context, payload RequestUpdateAccount) (data model.Account, err error) {
	search := RequestSearchAccount{
		Username: payload.Username,
	}

	acc, err := s.SearchOneAccount(ctx, search)
	if err != nil {
		if err == sql.ErrNoRows {
			err = fmt.Errorf("akun tidak ditemukan")
		}
		log.Println(err)
		return
	}

	if payload.FullName != "" {
		acc.FullName = payload.FullName
	}

	if payload.Password != "" {
		hashedPassword, _ := config.HashPassword(payload.Password)
		acc.Password = hashedPassword
	}

	if payload.Exp > 0 {
		acc.Exp = payload.Exp
	}

	if payload.Word > 0 {
		acc.Word = payload.Word
	}

	if payload.IsValidated == 1 {
		acc.IsValidated = payload.IsValidated
	}

	err = s.repository.UpdateAccount(ctx, acc)
	if err != nil {
		log.Println(err)
		return
	}

	data = acc
	return
}

func (s *service) DeleteAccount(ctx context.Context, payload RequestUpdateAccount) (err error) {
	err = s.repository.DeleteAccount(ctx, payload)
	return
}

func (s *service) AddExp(ctx context.Context, payload RequestAddExp) (data model.Account, err error) {

	search := RequestSearchAccount{
		Username: payload.Username,
	}

	acc, err := s.SearchOneAccount(ctx, search)
	if err != nil {
		if err == sql.ErrNoRows {
			err = fmt.Errorf("akun tidak ditemukan")
		}
		log.Println(err)
		return
	}

	requestUpdate := RequestUpdateAccount{
		Username: payload.Username,
		Exp:      acc.Exp + payload.Exp,
		Word:     acc.Word + 1,
	}

	data, err = s.UpdateAccount(ctx, requestUpdate)
	return
}

func (s *service) GetLeaderboard(ctx context.Context) (data []model.Account, err error) {
	reqSearch := RequestSearchAccount{
		SortExp: 1,
		Limit:   10,
	}

	data, err = s.repository.GetManyAccount(ctx, reqSearch)

	return
}

func (s *service) ResetPassword(ctx context.Context, payload RequestResetPassword) (data model.Account, err error) {
	newPassword := utils.GenerateRandomPassword()

	reqSearch := RequestSearchAccount{
		Username: payload.Username,
	}

	acc, err := s.SearchOneAccount(ctx, reqSearch)

	if err != nil {
		log.Println(err)
		return
	}

	if payload.Email != acc.Email {
		err = fmt.Errorf("username dan email tidak sesuai")
		log.Println(err)
		return
	}

	if acc.IsValidated == 0 {
		err = fmt.Errorf("akun ini belum divalidasi")
		return
	}

	reqUpdate := RequestUpdateAccount{
		Username: payload.Username,
		Password: newPassword,
	}

	data, err = s.UpdateAccount(ctx, reqUpdate)

	if err != nil {
		log.Println(err)
		return
	}

	err = utils.SendResetPasswordMail(acc.Email, acc.FullName, newPassword)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (s *service) ChangePassword(ctx context.Context, payload RequestChangePassword) (data model.Account, err error) {

	reqSearch := RequestSearchAccount{
		Username: payload.Username,
	}

	acc, err := s.SearchOneAccount(ctx, reqSearch)

	if err != nil {
		log.Println(err)
		return
	}

	if config.CheckPasswordHash(payload.OldPassword, acc.Password) {
		reqUpdate := RequestUpdateAccount{
			Username: payload.Username,
			Password: payload.NewPassword,
		}
		data, err = s.UpdateAccount(ctx, reqUpdate)
		if err != nil {
			log.Println(err)
			return
		}
		return
	}
	// log.Println(err)
	err = fmt.Errorf("password lama tidak sesuai")
	return
}

func (s *service) GetUserRank(c context.Context, payload RequestSearchAccount) (rank int, err error) {
	rank, err = s.repository.GetUserRank(c, payload)
	return
}
