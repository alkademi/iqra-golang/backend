package account

import (
	"iqra/src/config"
	"iqra/src/dictionary"
	"iqra/src/utils"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

func GetOneAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestSearchAccount

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.SearchOneAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func CreateAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestCreateAccount

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.CreateAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func Login() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestLogin

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.Login(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func UpdateAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestUpdateAccount

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.UpdateAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func DeleteAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestUpdateAccount

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		err = agent.DeleteAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(true))
	}
}

func ValidateAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestValidateAccount

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.ValidateAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func SelfAccount() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {

		token := strings.Split(ctx.Request().Header.Get("Authorization"), " ")[1]
		parsedJWT, _ := utils.DecodeJWT(token)

		payload := RequestSearchAccount{
			Username: parsedJWT["username"],
		}

		data, err := agent.SearchOneAccount(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func ResetPassword() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestResetPassword

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.ResetPassword(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func GetLeaderboard() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {

		data, err := agent.GetLeaderboard(ctx.Request().Context())

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func AddExp() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestAddExpLearned

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		reqWord := dictionary.RequestWordLearnedId{
			Username:   payload.Username,
			DictId:     payload.DictId,
			NativeLang: payload.NativeLang,
			TargetLang: payload.TargetLang,
		}

		err = dictAgent.AddOneWordLearned(ctx.Request().Context(), reqWord)
		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		reqExp := RequestAddExp{
			Username: payload.Username,
			Exp:      10,
		}
		_, err = agent.AddExp(ctx.Request().Context(), reqExp)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(true))
	}
}

func ChangePassword() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestChangePassword

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		if err = ctx.Validate(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorValidationResponse(err.Error()))
		}

		data, err := agent.ChangePassword(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}
