package constant

const (
	Host     = "postgres_iqra"
	Port     = 5432
	User     = "admin"
	Password = "password"
	DBname   = "iqra"
)

var (
	Lang = map[string]interface{}{
		"eng":  1,
		"indo": 2,
		"kor":  3,
	}
)
