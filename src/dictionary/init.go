package dictionary

import (
	"log"
)

var (
	dictAgent DictAgent
)

func init() {
	log.SetFlags(log.Llongfile)

	log.Println("Dictionary Initializing")

	dictAgent = NewService()

	log.Println("Dictionary Initialiazed")
}
