package dictionary

type RequestSearchDictByWord struct {
	NativeWord string `db:"native_word" json:"native_word"`
	NativeLang string `db:"native_lang" json:"native_lang"`
	TargetLang string `db:"target_lang" json:"target_lang"`
}

type RequestSearchDictById struct {
	DictId     int    `db:"dict_id" json:"dict_id"`
	NativeLang string `db:"native_lang" json:"native_lang"`
	TargetLang string `db:"target_lang" json:"target_lang"`
}

type RequestGetWordLearned struct {
	Username   string `param:"username" json:"username"`
	NativeLang string `param:"native_lang" json:"native_lang"`
	TargetLang string `param:"target_lang" json:"target_lang"`
}

type RequestWordLearnedId struct {
	Username   string `param:"username" json:"username"`
	DictId     int    `param:"dict_id" json:"dict_id"`
	NativeLang string `param:"native_lang" json:"native_lang"`
	TargetLang string `param:"target_lang" json:"target_lang"`
}
