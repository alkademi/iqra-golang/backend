package dictionary

import (
	"iqra/src/config"
	"net/http"

	"github.com/labstack/echo/v4"
)

func SearchWordsByWord() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestSearchDictByWord

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		data, err := dictAgent.SearchTenWordsByWord(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func SearchWordsById() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestSearchDictById

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		data, err := dictAgent.SearchTenWordsById(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func SearchWordsLearned() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestGetWordLearned

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		data, err := dictAgent.SearchAllWordsLearned(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}

func AddLearned() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestWordLearnedId

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		err = dictAgent.AddOneWordLearned(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(nil))
	}
}

func RemLearned() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestWordLearnedId

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		err = dictAgent.RemoveWordLearned(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(nil))
	}
}

func SearchRandomWords() echo.HandlerFunc {
	return func(ctx echo.Context) (err error) {
		var payload RequestGetWordLearned

		if err = ctx.Bind(&payload); err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorPayloadResponse())
		}

		data, err := dictAgent.SearchRandomWordsNotLearned(ctx.Request().Context(), payload)

		if err != nil {
			return ctx.JSON(http.StatusBadRequest,
				config.ErrorProcessingDataResponse(err.Error()))
		}

		return ctx.JSON(http.StatusOK, config.SuccessResponse(data))
	}
}
