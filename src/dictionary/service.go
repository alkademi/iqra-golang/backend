package dictionary

import (
	"context"
	"database/sql"
	"fmt"
	"iqra/src/config"
	"iqra/src/model"
	"log"
)

type service struct {
	repository *repository
}

type DictAgent interface {
	SearchTenWordsByWord(ctx context.Context, payload RequestSearchDictByWord) (data []model.Dictionary, err error)
	SearchTenWordsById(ctx context.Context, payload RequestSearchDictById) (data []model.Dictionary, err error)
	SearchAllWordsLearned(ctx context.Context, payload RequestGetWordLearned) (data []model.Dictionary, err error)
	AddOneWordLearned(ctx context.Context, payload RequestWordLearnedId) (err error)
	RemoveWordLearned(ctx context.Context, payload RequestWordLearnedId) (err error)
	SearchRandomWordsNotLearned(ctx context.Context, payload RequestGetWordLearned) (data []model.Dictionary, err error)
}

func NewService() DictAgent {
	db := config.GetDB()
	repo := newRepository(db)
	return &service{repository: repo}
}

func (s *service) SearchTenWordsByWord(ctx context.Context, payload RequestSearchDictByWord) (data []model.Dictionary, err error) {
	data, err = s.repository.GetTenWordsByWord(ctx, payload)
	return
}

func (s *service) SearchTenWordsById(ctx context.Context, payload RequestSearchDictById) (data []model.Dictionary, err error) {
	data, err = s.repository.GetTenWordsById(ctx, payload)
	return
}

func (s *service) SearchAllWordsLearned(ctx context.Context, payload RequestGetWordLearned) (data []model.Dictionary, err error) {
	data, err = s.repository.GetWordsLearned(ctx, payload)
	return
}

func (s *service) AddOneWordLearned(ctx context.Context, payload RequestWordLearnedId) (err error) {
	// Look for duplicate
	search_learned := RequestWordLearnedId{
		Username:   payload.Username,
		DictId:     payload.DictId,
		NativeLang: payload.NativeLang,
		TargetLang: payload.TargetLang,
	}

	_, err = s.repository.GetOneWordLearned(ctx, search_learned)

	if err != nil && err != sql.ErrNoRows {
		log.Println(err)
		return
	}

	if err == nil {
		err = fmt.Errorf("kata sudah dipelajari")
		log.Println(err)
		return
	}

	// Add Word
	err = s.repository.AddWordLearned(ctx, payload)
	if err != nil {
		log.Println(err)
	}
	return
}

func (s *service) RemoveWordLearned(ctx context.Context, payload RequestWordLearnedId) (err error) {
	// Look for word
	search_learned := RequestWordLearnedId{
		Username:   payload.Username,
		DictId:     payload.DictId,
		NativeLang: payload.NativeLang,
		TargetLang: payload.TargetLang,
	}

	_, err = s.repository.GetOneWordLearned(ctx, search_learned)

	if err != nil && err == sql.ErrNoRows {
		log.Println(err)
		return
	}

	// Remove Word
	err = s.repository.RemWordLearned(ctx, payload)
	if err != nil {
		log.Println(err)
	}
	return
}

func (s *service) SearchRandomWordsNotLearned(ctx context.Context, payload RequestGetWordLearned) (data []model.Dictionary, err error) {
	data, err = s.repository.GetRandomWordsNotLearned(ctx, payload)
	return
}
