package dictionary

import (
	"context"
	"fmt"
	"log"

	"iqra/src/constant"
	"iqra/src/model"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

type repository struct {
	DB *sqlx.DB
}

func newRepository(db *sqlx.DB) *repository {
	return &repository{DB: db}
}

func (r *repository) GetTenWordsByWord(c context.Context, req RequestSearchDictByWord) (res []model.Dictionary, err error) {
	var dictionary string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		dictionary = "dictionary_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		dictionary = "dictionary_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		dictionary = "dictionary"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query, args, _ := psq.Select("*").
		From(dictionary).
		Where("native_word LIKE ? and native_lang = ? and target_lang = ?", fmt.Sprint("%", req.NativeWord, "%"), req.NativeLang, req.TargetLang).
		Limit(10).ToSql()

	err = r.DB.SelectContext(c, &res, query, args...)

	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (r *repository) GetTenWordsById(c context.Context, req RequestSearchDictById) (res []model.Dictionary, err error) {

	nat := constant.Lang[req.NativeLang]
	tar := constant.Lang[req.TargetLang]
	id1 := fmt.Sprintf("%d%d%d", nat, tar, req.DictId)
	id2 := fmt.Sprintf("%d%d%d", nat, tar, req.DictId+10)
	var dictionary string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		dictionary = "dictionary_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		dictionary = "dictionary_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		dictionary = "dictionary"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query, args, _ := psq.Select("*").
		From(dictionary).
		Where("dict_id > ? and dict_id <= ? and native_lang = ? and target_lang = ?", id1, id2, req.NativeLang, req.TargetLang).
		Limit(10).ToSql()

	err = r.DB.SelectContext(c, &res, query, args...)

	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (r *repository) GetOneWordLearned(c context.Context, req RequestWordLearnedId) (res model.Learned, err error) {
	var learned string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		learned = "learned_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		learned = "learned_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		learned = "learned"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query := psq.Select("*").
		Where("username = ? and dict_id = ?", req.Username, req.DictId).
		From(learned)

	query = query.RunWith(r.DB)

	err = query.Scan(&res.Username, &res.DictId)

	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) GetWordsLearned(c context.Context, req RequestGetWordLearned) (res []model.Dictionary, err error) {
	var learned string
	var dictionary string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		learned = "learned_kor_indo"
		dictionary = "dictionary_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		learned = "learned_kor_eng"
		dictionary = "dictionary_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		learned = "learned"
		dictionary = "dictionary"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	rows, err := psq.Select("dict_id, native_word, target_word, target_definition, native_lang, target_lang").
		From(learned).
		InnerJoin(fmt.Sprint(dictionary, " USING (dict_id)")).
		Where("username = ? and native_lang = ? and target_lang = ?", req.Username, req.NativeLang, req.TargetLang).
		RunWith(r.DB).Query()

	for rows.Next() {
		var each = model.Dictionary{}
		err = rows.Scan(&each.DictId, &each.NativeWord, &each.TargetWord, &each.TargetDefinition, &each.NativeLang, &each.TargetLang)

		if err != nil {
			log.Fatal(err)
			return
		}

		res = append(res, each)
	}

	if err != nil {
		log.Fatal(err)
	}

	return
}

func (r *repository) AddWordLearned(c context.Context, req RequestWordLearnedId) (err error) {
	var learned string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		learned = "learned_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		learned = "learned_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		learned = "learned"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query, args, _ := psq.Insert(learned).
		Columns(
			"username",
			"dict_id").
		Values(
			req.Username,
			req.DictId).
		ToSql()

	tx, err := r.DB.BeginTx(c, nil)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = tx.ExecContext(c, query, args...)
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) RemWordLearned(c context.Context, req RequestWordLearnedId) (err error) {
	var learned string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		learned = "learned_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		learned = "learned_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		learned = "learned"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	query, args, _ := psq.Delete(learned).
		Where("username = ? and dict_id = ?", req.Username, req.DictId).
		ToSql()

	tx, err := r.DB.BeginTx(c, nil)
	if err != nil {
		log.Println(err)
		return
	}

	_, err = tx.ExecContext(c, query, args...)
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return
	}

	err = tx.Commit()
	if err != nil {
		log.Println(err)
	}

	return
}

func (r *repository) GetRandomWordsNotLearned(c context.Context, req RequestGetWordLearned) (res []model.Dictionary, err error) {
	var learned string
	var dictionary string

	if (req.NativeLang == "kor" && req.TargetLang == "indo") || (req.NativeLang == "indo" && req.TargetLang == "kor") {
		learned = "learned_kor_indo"
		dictionary = "dictionary_kor_indo"
	} else if (req.NativeLang == "kor" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "kor") {
		learned = "learned_kor_eng"
		dictionary = "dictionary_kor_eng"
	} else if (req.NativeLang == "indo" && req.TargetLang == "eng") || (req.NativeLang == "eng" && req.TargetLang == "indo") {
		learned = "learned"
		dictionary = "dictionary"
	}

	psq := sq.StatementBuilder.PlaceholderFormat(sq.Dollar)
	rows, err := psq.Select("dict_id, native_word, target_word, target_definition, native_lang, target_lang").
		From(learned).
		RightJoin(fmt.Sprint(dictionary, " USING (dict_id)")).
		Where("username != ? or username is null and native_lang = ? and target_lang = ?", req.Username, req.NativeLang, req.TargetLang).
		OrderBy("RANDOM()").
		Limit(10).
		RunWith(r.DB).Query()

	for rows.Next() {
		var each = model.Dictionary{}
		err = rows.Scan(&each.DictId, &each.NativeWord, &each.TargetWord, &each.TargetDefinition, &each.NativeLang, &each.TargetLang)

		if err != nil {
			log.Fatal(err)
			return
		}

		res = append(res, each)
	}

	if err != nil {
		log.Fatal(err)
	}

	return
}
