package config

import (
	"fmt"
	"log"
	"os"

	"iqra/src/constant"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

var (
	database *sqlx.DB
)

func init() {
	log.SetFlags(log.Llongfile)
	log.Println("Connection Pool Initializing")

	InitDB()

	log.Println("Connection Pool Initialiazed")
}

// DBInit create connection to database
func InitDB() {
	// psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", constant.Host, constant.Port, constant.User, constant.Password, constant.DBname)
	url := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		constant.User,
		constant.Password,
		constant.Host,
		constant.Port,
		constant.DBname)

	db, err := sqlx.Open("postgres", url)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	database = db
}

func GetDB() *sqlx.DB {
	return database
}

func GetEnvVar(key string) string {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
	return os.Getenv(key)
}
