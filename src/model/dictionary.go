package model

type Dictionary struct {
	DictId           int     `db:"dict_id" json:"dict_id"`
	NativeWord       string  `db:"native_word" json:"native_word"`
	TargetWord       *string `db:"target_word" json:"target_word"`
	TargetDefinition string  `db:"target_definition" json:"target_definition"`
	NativeLang       string  `db:"native_lang" json:"native_lang"`
	TargetLang       string  `db:"target_lang" json:"target_lang"`
}
