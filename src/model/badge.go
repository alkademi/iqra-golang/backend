package model

type Badge struct {
	Id      int    `param:"badge_id" json:"badge_id"`
	Name    string `param:"name" json:"name"`
	Desc    string `param:"description" json:"description"`
	Model   []byte `param:"model" json:"model"`
	ExpReq  int    `param:"exp_req" json:"exp_req"`
	WordReq int    `word:"word_req" json:"word_req"`
}
