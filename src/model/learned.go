package model

type Learned struct {
	Username string `param:"username" json:"username"`
	DictId   int    `param:"dict_id" json:"dict_id"`
}
