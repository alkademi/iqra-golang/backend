package model

type Achievement struct {
	Username string `param:"username" json:"username"`
	BadgeId  int    `param:"badge_id" json:"badge_id"`
}
