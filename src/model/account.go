package model

type Account struct {
	Username    string `db:"username" json:"username"`
	Password    string `db:"password" json:"password"`
	FullName    string `db:"full_name" json:"full_name"`
	Email       string `db:"email" json:"email"`
	BirthDate   string `db:"birth_date" json:"birth_date"`
	Exp         int32  `db:"exp" json:"exp"`
	Word        int32  `db:"word_learned" json:"word_learned"`
	IsValidated int    `db:"is_validated" json:"is_validated"`
	RegisToken  string `db:"regis_token" json:"regis_token"`
	Rank        int    `db:"rank" json:"rank"`
}
