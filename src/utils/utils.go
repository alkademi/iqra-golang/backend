package utils

import (
	"crypto/rand"
	"fmt"
	"html/template"
	"iqra/src/config"
	"log"
	"strings"

	"github.com/golang-jwt/jwt"
	mail "github.com/xhit/go-simple-mail/v2"
)

func GenerateRegisToken() string {
	b := make([]byte, 4)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func GenerateRandomPassword() string {
	b := make([]byte, 5)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func SendAccountValidationMail(to_email string, name string, token string) (err error) {
	server := mail.NewSMTPClient()
	server.Host = "smtp.gmail.com"
	server.Port = 587
	server.Username = config.GetEnvVar("MAILER_EMAIL")
	server.Password = config.GetEnvVar("MAILER_PASSWORD")
	server.Encryption = mail.EncryptionTLS

	smtpClient, err := server.Connect()
	if err != nil {
		return
	}

	const htmlBody = `
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		</head>
		<body>
			<div style="width:100%;height:2.5rem;background-color:#5840BA"></div>
			<img src="https://drive.google.com/uc?id=1YSkqZwII0-Fc_Os5xmDnTwg_0XusUjeN" style="width:8rem;height:auto;margin-top:1rem"/>
			<h1 style="font-family:Arial;font-weight:600;font-size:1.3rem;margin-top:2rem;color:#212121">Validasi Akun Anda</h1>
			<p style="font-family:Arial;font-weight:400;font-size:1rem;color:#212121">Halo {{.Name}}, segera validasi akun kamu dengan memasukkan kode dibawah ini pada aplikasi Iqra.id!</p>
			<p style="font-family:Arial;font-weight:600;font-size:1.3rem;color:#212121">{{.Token}}</p>
		</body>
	</html>
	`

	data := map[string]interface{}{
		"Name":  name,
		"Token": token,
	}

	t := template.Must(template.New("email").Parse(htmlBody))
	builder := &strings.Builder{}

	if err = t.Execute(builder, data); err != nil {
		log.Println(err)
		return
	}
	strTemplate := builder.String()

	// Create email
	email := mail.NewMSG()
	email.AddTo(to_email)
	email.SetSubject("Validasi Akun Iqra.id")

	email.SetBody(mail.TextHTML, strTemplate)

	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		return
	}
	return
}

func SendResetPasswordMail(to_email string, name string, newPassword string) (err error) {
	server := mail.NewSMTPClient()
	server.Host = "smtp.gmail.com"
	server.Port = 587
	server.Username = config.GetEnvVar("MAILER_EMAIL")
	server.Password = config.GetEnvVar("MAILER_PASSWORD")
	server.Encryption = mail.EncryptionTLS

	smtpClient, err := server.Connect()
	if err != nil {
		return
	}

	const htmlBody = `
	<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		</head>
		<body>
			<div style="width:100%;height:2.5rem;background-color:#5840BA"></div>
			<img src="https://drive.google.com/uc?id=1YSkqZwII0-Fc_Os5xmDnTwg_0XusUjeN" style="width:8rem;height:auto;margin-top:1rem"/>
			<h1 style="font-family:Arial;font-weight:600;font-size:1.3rem;margin-top:2rem;color:#212121">Reset Password Akun Anda</h1>
			<p style="font-family:Arial;font-weight:400;font-size:1rem;color:#212121">Halo {{.Name}}, berikut adalah password baru akun kamu:</p>
			<p style="font-family:Arial;font-weight:600;font-size:1.3rem;color:#212121">{{.NewPassword}}</p>
		</body>
	</html>
	`

	data := map[string]interface{}{
		"Name":        name,
		"NewPassword": newPassword,
	}

	t := template.Must(template.New("email").Parse(htmlBody))
	builder := &strings.Builder{}

	if err = t.Execute(builder, data); err != nil {
		log.Println(err)
		return
	}
	strTemplate := builder.String()

	// Create email
	email := mail.NewMSG()
	email.AddTo(to_email)
	email.SetSubject("Reset Password Akun Iqra.id")

	email.SetBody(mail.TextHTML, strTemplate)

	// Send email
	err = email.Send(smtpClient)
	if err != nil {
		return
	}
	return
}

func DecodeJWT(token string) (auth map[string]string, err error) {
	claims := jwt.MapClaims{}
	_, err = jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.GetEnvVar("JWT_SIGNATURE_KEY")), nil
	})

	if err != nil {
		log.Println(err)
		return
	}

	auth = make(map[string]string)
	for key, val := range claims {
		auth[key] = val.(string)
	}
	return
}
