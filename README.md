# IF3250_2022_32_IQRA_Backend

## How To Access Deployed API
1. API is deployed at http://128.199.247.11:8080/ (endpoint sample : http://128.199.247.11:8080/hello)
2. Visit http://128.199.247.11:5050/ to access pgadmin
- Email : email@domain.com
- Password : password

## How To Run Locally with Docker

1. Install Docker
2. Run `docker build -t iqra-api .`
3. Run `docker-compose -f docker-compose.yaml up`

### How to Access Database Server
1. Access `localhost:5050` to open pgadmin
2. Click `Add new Server`
3. Server settings :
- name : postgres
- host name : postgres
- port : 5432
- maintenance database : iqra
- username : admin
- password : password
4. Save
