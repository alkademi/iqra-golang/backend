CREATE TABLE public.account (
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    full_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    birth_date date,
    exp integer,
    word_learned integer,
    is_validated integer,
    regis_token character varying(255)
);


CREATE TABLE public.achievement (
    username character varying(255) NOT NULL,
    badge_id integer NOT NULL
);


CREATE TABLE public.badge (
    badge_id integer NOT NULL,
    name character varying(255),
    description character varying(255),
    model bytea,
    exp_req integer,
    word_req integer
);

CREATE TABLE public.dictionary (
    dict_id integer NOT NULL,
    native_word character varying(255),
    target_word character varying(255),
    target_definition character varying(65535),
    native_lang character varying(10),
    target_lang character varying(10)
);

CREATE TABLE public.learned (
    username character varying(255) NOT NULL,
    dict_id integer NOT NULL
);


ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (username);

ALTER TABLE ONLY public.badge
    ADD CONSTRAINT badge_pkey PRIMARY KEY (badge_id);

ALTER TABLE ONLY public.dictionary
    ADD CONSTRAINT dictionary_pkey PRIMARY KEY (dict_id);


ALTER TABLE ONLY public.achievement
    ADD CONSTRAINT achievement_badge_id_fkey FOREIGN KEY (badge_id) REFERENCES public.badge(badge_id);

ALTER TABLE ONLY public.achievement
    ADD CONSTRAINT achievement_username_fkey FOREIGN KEY (username) REFERENCES public.account(username);

ALTER TABLE ONLY public.learned
    ADD CONSTRAINT learned_dict_id_fkey FOREIGN KEY (dict_id) REFERENCES public.dictionary(dict_id);

ALTER TABLE ONLY public.learned
    ADD CONSTRAINT learned_username_fkey FOREIGN KEY (username) REFERENCES public.account(username);