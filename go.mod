module iqra

go 1.17

require github.com/labstack/echo/v4 v4.7.1

require (
	github.com/lib/pq v1.10.4
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

require (
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-test/deep v1.0.8 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/toorop/go-dkim v0.0.0-20201103131630-e1cd1a0a5208 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
)

require (
	github.com/Masterminds/squirrel v1.5.2
	github.com/go-playground/validator/v10 v10.10.1
	github.com/golang-jwt/jwt/v4 v4.3.0
	github.com/jmoiron/sqlx v1.3.4
	// github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.4.0
	github.com/labstack/gommon v0.3.1 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	github.com/xhit/go-simple-mail/v2 v2.11.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // direct
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/text v0.3.7 // indirect
)
