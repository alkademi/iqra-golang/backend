package test

import (
	"iqra/src/account"
	"iqra/src/dictionary"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func TestGetAccount(t *testing.T) {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}
	req := httptest.NewRequest(http.MethodPost, "/account/get_one", strings.NewReader(accountGetJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := account.GetOneAccount()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestLoginAccount(t *testing.T) {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}
	req := httptest.NewRequest(http.MethodPost, "/login", strings.NewReader(accountLoginJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := account.Login()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestAddExpAccount(t *testing.T) {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}
	req := httptest.NewRequest(http.MethodPost, "/account/add_exp", strings.NewReader(dictAddRemLearned))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := account.AddExp()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)

	}

	// delete learned word
	req = httptest.NewRequest(http.MethodPost, "/dict/rem-learned", strings.NewReader(dictAddRemLearned))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)

	res = rec.Result()
	defer res.Body.Close()

	f = dictionary.RemLearned()
	f(c)
}

func TestChangePwdAccount(t *testing.T) {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}
	req := httptest.NewRequest(http.MethodPost, "/account/change_password", strings.NewReader(accountChangePwdJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := account.ChangePassword()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}

	// return password
	req = httptest.NewRequest(http.MethodPost, "/account/change_password", strings.NewReader(accountChangePwdBackJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec = httptest.NewRecorder()
	c = e.NewContext(req, rec)

	res = rec.Result()
	defer res.Body.Close()

	f = account.ChangePassword()
	f(c)
}

func TestGetLeaderboard(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/leaderboard", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := account.GetLeaderboard()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
