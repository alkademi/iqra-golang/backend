package test

var (
	// ACCOUNT TEST CASES

	//get account
	accountGetJSON = `{
		"username":"iqra_test",
		"email":"13519152@std.stei.itb.ac.id"
	}`
	//change password
	accountChangePwdJSON = `{
		"username":"iqra_test",
		"old_password":"kagomekagome",
		"new_password":"kagomekagomekagome"
	}`
	accountChangePwdBackJSON = `{
		"username":"iqra_test",
		"old_password":"kagomekagomekagome",
		"new_password":"kagomekagome"
	}`
	//login
	accountLoginJSON = `{
		"username":"iqra_test",
		"password":"kagomekagome"
	}`

	//DICTIONARY TEST CASES

	//get dict by word
	dictWordJSON = `{
		"native_word" : "hamster", 
		"native_lang" : "eng", 
		"target_lang" : "indo"
	}`
	dictWordResult = `{"code":200,"message":"Success","data":[{"dict_id":129368,"native_word":"hamster","target_word":null,"target_definition":"kb. semacam tupai.\n","native_lang":"eng","target_lang":"indo"}]}
`
	//get dict by id
	dictIdJSON = `{
		"dict_id" : 1,
		"native_lang" : "eng",
		"target_lang" : "indo"
	}`
	//get word learned
	dictGetLearned = `{
		"username" : "iqra32",
		"native_lang" : "eng",
		"target_lang" : "indo"
	}`
	//add and remove word learned
	dictAddRemLearned = `{
		"username" : "iqra_test",
		"dict_id" : 121,
		"native_lang" : "eng",
		"target_lang" : "indo"
	}`
	//get flashcard
	dictGetFlashcard = `{
		"username" : "iqra32",
		"native_lang" : "eng",
		"target_lang" : "indo"
	}`
)
