package test

import (
	"iqra/src/dictionary"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestGetWordDictionary(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/get-words", strings.NewReader(dictWordJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.SearchWordsByWord()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, dictWordResult, rec.Body.String())
	}
}

func TestGetIdDictionary(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/get-words", strings.NewReader(dictIdJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.SearchWordsById()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestGetLearned(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/get-learned", strings.NewReader(dictGetLearned))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.SearchWordsLearned()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestAddLearned(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/add-learned", strings.NewReader(dictAddRemLearned))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.AddLearned()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestRemLearned(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/rem-learned", strings.NewReader(dictAddRemLearned))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.RemLearned()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}

func TestGetFlashcard(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/dict/get-flashcard-random", strings.NewReader(dictGetFlashcard))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	res := rec.Result()
	defer res.Body.Close()

	f := dictionary.SearchRandomWords()
	if assert.NoError(t, f(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}
